from django.db import models

# Create your models here.

# --- examples ---


# class IntegerBox(models.Model):
#     integer = models.IntegerField()

#     def integer_value(self):
#         return self.integer


# --- project ---


class Client(models.Model):
    first_name = models.CharField(max_length=400)
    last_name = models.CharField(max_length=400)
    email = models.EmailField(max_length=254)
    associated_card_number = models.CharField(max_length=16, null=True)
    # debit or credit card
    associated_card_type = models.CharField(max_length=30, null=True)
    score = models.FloatField()

    def __str__(self) -> str:
        full_name = self.first_name + " " + self.last_name
        return full_name


class Shopper(models.Model):
    first_name = models.CharField(max_length=400)
    last_name = models.CharField(max_length=400)
    rut = models.IntegerField()
    bank = models.CharField(max_length=400)
    bank_account_number = models.CharField(max_length=16)
    email = models.EmailField(max_length=254)
    score = models.FloatField()

    def __str__(self) -> str:
        full_name = self.first_name + " " + self.last_name
        return full_name


class DeliveryAddress(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=400)
    tag = models.CharField(max_length=400)
    address = models.CharField(max_length=400)
    lat_coord = models.FloatField()
    lon_coord = models.FloatField()
    misc = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self) -> str:
        address_descr = f"Name: {self.name}, Address: {self.address}"
        return address_descr


class Store(models.Model):
    name = models.CharField(max_length=400)
    category = models.CharField(max_length=400)
    address = models.CharField(max_length=400)
    lat_coord = models.FloatField()
    lon_coord = models.FloatField()

    def __str__(self) -> str:
        store_descr = f"Name: {self.name}, Address: {self.address}"
        return store_descr


class Product(models.Model):
    name = models.CharField(max_length=400)
    price = models.IntegerField()
    category = models.CharField(max_length=400)
    brand = models.CharField(max_length=400)
    stores = models.ManyToManyField(Store)

    def __str__(self) -> str:
        product_descr = f"Name: {self.name}, Brand: {self.brand}"
        return product_descr


class Order(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    shopper = models.ForeignKey(Shopper, on_delete=models.CASCADE)
    delivery_address = models.ForeignKey(DeliveryAddress, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    deliver_at = models.DateTimeField()
    # payment options
    payment = models.CharField(max_length=400)
    # order completion state
    completion_state = models.CharField(max_length=400)
    score = models.FloatField(null=True)
    products = models.ManyToManyField(Product)

    def __str__(self) -> str:
        order_descr = f"Client: {self.client_id}, Shopper: {self.shopper_id}, Created: {self.created_at}"
        return order_descr

    @property
    def price(self):
        products = self.products.all()
        price = 0
        for product in products:
            price = price + product.price
        return price * 1.1


class Route(models.Model):
    lat_coord = models.FloatField()
    lon_coord = models.FloatField()
    timestamp = models.DateTimeField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    class Meta:
        ordering = ["timestamp"]

    def __str__(self) -> str:
        route_resume = f"Order Id: {self.order_id}, Timestamp: {self.timestamp}"
        return route_resume


# Background data of shoppers that apply to insta carrito.
class BackgroundCheck(models.Model):
    shopper = models.ForeignKey(Shopper, on_delete=models.CASCADE)
    age = models.IntegerField()
    criminal_record = models.FileField(upload_to="criminal_record/")
    profile_headshot = models.FileField(upload_to="profile_headshot/")
    id_photo = models.FileField(upload_to="id_photo/")
    """ status of background check procedure:
        processing, accepted, rejected"""
    status_of_procedure = models.CharField(max_length=400)

    def __str__(self) -> str:
        return f"Shopper Id: {self.shopper_id}"


# Payments of orders made by clients
class ClientPayment(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    amount = models.IntegerField(null=True)
    payment_method = models.CharField(max_length=400, null=True)
    """ status of transaction:
        pending, accepted, declined"""
    status = models.CharField(max_length=400)

    def __str__(self) -> str:
        payment_resume = f"Client Id: {self.client_id}, Order Id: {self.order_id}"
        return payment_resume


# Payments of orders bought by shoppers
class ShopperPayment(models.Model):
    shopper = models.ForeignKey(Shopper, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    amount = models.IntegerField(null=True)
    bank = models.CharField(max_length=400, null=True)
    bank_account_number = models.CharField(max_length=16, null=True)
    """ status of transaction:
        pending, accepted, declined"""
    status = models.CharField(max_length=400)

    def __str__(self) -> str:
        payment_resume = (
            f"Shopper Id: {self.shopper_id}, Order Id: {self.order_id}"
        )
        return payment_resume
