from model_bakery.recipe import Recipe, foreign_key, related
from datetime import datetime
from django.utils.timezone import make_aware
from app.models import (
    Client,
    Shopper,
    Route,
    Store,
    Product,
    DeliveryAddress,
    Order,
    BackgroundCheck,
    ClientPayment,
    ShopperPayment,
)

example_client = Recipe(
    Client,
    first_name="client_name",
    last_name="client_lastname",
    email="example_client@gmail.com",
    associated_card_number="1234567890123456",
    associated_card_type="debit",
    score=4.8,
)

example_shopper = Recipe(
    Shopper,
    first_name="shopper_name",
    last_name="shopper_lastname",
    rut=19687866,
    bank="banco estado",
    bank_account_number="19687866",
    email="example_shopper@gmail.com",
    score=4.1,
)

example_delivery_address = Recipe(
    DeliveryAddress,
    client=foreign_key(example_client),
    name="Costanera Center",
    tag="work",
    address="Av. Andres Bello 2425",
    lat_coord=-33.4177448,
    lon_coord=-70.6065351,
)

example_store = Recipe(
    Store,
    name="La Teteria",
    category="tea & supplements",
    address="Sta. Magdalena 86",
    lat_coord=-33.4206074,
    lon_coord=-70.6798925,
)

example_product1 = Recipe(
    Product,
    stores=related(example_store),
    price=1350,
)

example_product2 = Recipe(Product, stores=related(example_store), price=500)

example_order = Recipe(
    Order,
    client=foreign_key(example_client),
    shopper=foreign_key(example_shopper),
    delivery_address=foreign_key(example_delivery_address),
    products=related(example_product1, example_product2),
    created_at=make_aware(
        datetime.strptime("23/2/2020 11:12:22.234513", "%d/%m/%Y %H:%M:%S.%f")
    ),
    deliver_at=make_aware(
        datetime.strptime("23/2/2020 11:12:22.234513", "%d/%m/%Y %H:%M:%S.%f")
    ),
)

example_route = Recipe(
    Route,
    lat_coord=-33.457607980702,
    lon_coord=-70.664476251628,
    timestamp=make_aware(
        datetime.strptime("23/2/2020 11:12:22.234513", "%d/%m/%Y %H:%M:%S.%f")
    ),
    order=foreign_key(example_order),
)

example_background_check = Recipe(
    BackgroundCheck,
    shopper=foreign_key(example_shopper),
    age=26,
    status_of_procedure="accepted",
)

example_client_payment = Recipe(
    ClientPayment,
    client=foreign_key(example_client),
    created_at=make_aware(
        datetime.strptime("23/2/2020 11:12:22.234513", "%d/%m/%Y %H:%M:%S.%f")
    ),
    order=foreign_key(example_order),
    status="pending",
)

example_shopper_payment = Recipe(
    ShopperPayment,
    shopper=foreign_key(example_shopper),
    created_at=make_aware(
        datetime.strptime("23/2/2020 11:12:22.234513", "%d/%m/%Y %H:%M:%S.%f")
    ),
    order=foreign_key(example_order),
    status="pending",
)
